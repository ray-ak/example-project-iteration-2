.. SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
.. SPDX-License-Identifier: MIT


Developer Guide
===============

Here you can find out how you can contribute to the further development.

Getting Started
---------------

We assume that you have installed the following tools:

- `Python >= 3.6.1 <https://www.python.org/downloads/>`__
- `Poetry >= 1.0.5 <https://python-poetry.org/docs/#installation>`__
- `Git command line client <https://git-scm.com/downloads>`__
- A bash shell including GNU make (>=4.3)

**1. Checkout the latest source code from the repository**

.. code-block:: bash

   git clone https://gitlab.dlr.de/dlr-se-workshops/software-development-for-teams/sample-calculator.git


**2. Install the development dependencies**

.. code-block:: bash

   cd sample-calculator
   poetry install

**3. Check that everything works fine**

.. code-block:: bash

   make audit

**4. Start with your work :-)**


System Overview
---------------

The Sample Calculator functionality is implemented by its main package :py:meth:`sample_calculator`.
The package is structured as follows:

-  The sub-package :py:meth:`sample_calculator.inputs` provides access to the implemented input methods.
   An input method describes from which data source the sample values are retrieved.

-  The sub-package :py:meth:`sample_calculator.calculations` provides access to the implemented calculations.
   A calculation is always performed on a sequence of numeric values and returns a single value.

-  The module :py:meth:`sample_calculator.main` is the central entry point of the application.
   It initializes input methods, calculations, and the logging capabilities.
   In addition, it implements the command-line interface.

Below you find the top-level repository layout:

::

|-- docs # Location of the user and developer documentation
|-- LICENSES # License texts
|-- sample_calculator # Python source code(see above)
|-- tests # Tests
|-- .gitlab-ci.yml # Implements the GitLab CI build pipeline
|-- .gitignore # Defines files that should not be not committed
|-- CHANGELOG.md # Release documentation
|-- CONTRIBUTING.md # Explains how others can contribute
|-- README.md # Overview of the most important information
|-- pyproject.toml # Describes project metadata, dependencies, tool configuration
|-- poetry.lock # Pins the currently used dependency configuration
|-- Makefile # Provides some wrapper build commands to automate certain tasks / checks
|-- pytest.ini, .flake8 # Additional configuration files for pytest and flake8


Change Management
-----------------

In the following, we describe how software changes (e.g., bug fixes, new features, enhancements) are performed until they are available in a stable release version.

In general, we follow an iterative approach in which we regularly discuss and prioritize change requests originating from different sources.
There is no fixed iteration duration.
However, we strive for 4 to 6 week iterations.
The different change requests and tasks are captured as (GitLab) issues.
On their basis, the work is planned and tracked.

A typical iteration is performed as follows:

- **Iteration Kick-Off:** A planning meeting is performed to kick-off the iteration.
  In this context, the contributors discuss the scope of the iteration and related issues.
  The issues are selected from the prioritized list of all open issues (backlog).
  At the end, all foreseen issues are assigned and related to the current milestone.

- **Implementation:** In parallel, the contributors work on the different issues.
  For details, please see below.

- **Iteration Closing:** The results of the iteration are validated via a live demonstration on the basis of a release package installed in a production-like environment.
  In addition, improvements of the overall development workflow are discussed.
  The findings and results are captured in the issue tracker. Finally, the milestone is closed.


Contributing a Change
^^^^^^^^^^^^^^^^^^^^^

**Basic principles:**

- There is only one long-lived branch named ``master``.

- Short-lived ``feature branches`` are used to develop features, fix bugs etc.

- Feature branches are merged into the ``master`` branch via ``merge requests``.

- A merge request has to pass the integration build and has to be approved by a reviewer to get integrated into the ``master``.

- Feature branches are integrated into the ``master`` via explicit ``merge commits``.


**Perform the following steps to introduce a new change into the master branch:**

- Create an issue describing the purpose of the change.

- Create a merge request from the related issue (using the merge template ``Review``),
  pull the new branch into your local Git repository,
  and work on it.

  .. IMPORTANT::

     - Follow the :ref:`implementation guidelines <impl-guidelines>`.

     - Regularly push changes to GitLab to save and back up your work.

     - Regularly pull the ``master`` branch and rebase it with your feature branch to be up-to-date.

     - Write useful Git commit messages.

       - Please use this commit message template:
         ::

           GIT SUBJECT

           * Detail 1
           * Detail 2
           * Detail N


       - A Git subject should complete the following sentence:
         If applied, this commit will ``GIT SUBJECT``, Example: If applied, this commit will ``Add ray tracing support``.

- When your contribution is ready:
  Clean up your commits, remove the ``WIP`` (work in progress) tag, and assign a reviewer.

  .. IMPORTANT::

     To make the review process sufficiently work for both sides, please provide merge requests focused on ONE specific issue (e.g., one bug fix, one feature) and avoid merge requests > 500 changed lines of code.

- To get your changes merged into the ``master`` branch:
  The integration build has to run successfully and a reviewer has to approve your changes.
  Further hints on the review procedure are indicated in the merge request description.

- When the merge request has been approved:
  Merge the changes into master and delete the feature branch.


Creating a Release
^^^^^^^^^^^^^^^^^^

**Basic principles:**

- We are using `Semantic Versioning <https://semver.org/>`__ for release numbers (e.g., 1.0.0).
- We follow `Keep a Changelog <https://keepachangelog.com/>`__ for the release documentation.
- Releases are planned via GitLab issues and milestones.
- Releases are created on short-lived release branches following the naming template ``release/MAJOR.MINOR.PATCH``.
- Releases are tagged using the following naming convention ``MAJOR.MINOR.PATCH``.
- The build pipeline creates the release package on the basis of a created release tag.

.. IMPORTANT::

   Please create an issue of type `Release` and follow the steps outlined their to create a new release.


.. _impl-guidelines:

Implementation Guidelines
-------------------------

- Please follow the code style defined by `PEP8 <https://www.python.org/dev/peps/pep-0008/>`__.
  We use ``flake8`` to check PEP8 compliance.
  Additional checks are performed via ``black`` and ``isort``.

- Please follow the `REUSE Specification 3.0 <https://reuse.software/spec/>`__ to add copyright and license hints.
  Particularly, make sure that **every file** contains proper ``SPDX`` tags:

.. code-block::

   SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
   SPDX-License-Identifier: MIT

- Please make sure that you check the license of newly added dependencies for compatibility.
  In general, we want to make sure that no specific dependencies are required to **use** this package.
  I.e., it should only require a basic Python interpreter.

- We use ``pytest`` for testing, please follow these guidelines:

  - Add tests for every newly introduced / changed functionality.
    Please follow the `pytest naming conventions <https://docs.pytest.org/en/latest/goodpractices.html#conventions-for-python-test-discovery>`__.

  - For user-visible features, you should extend ``end_to_end_test.py`` which checks the command-line tool directly.

  - Avoid to reduce the overall code coverage.
    The check target will fail, if the code coverage drops below 80%.

- We use ``flake8`` and ``bandit`` for static code analysis, please follow these guidelines:

  - Do not introduce new violations.

  - If you are sure that these tools report a false-positive, you might exclude them via their configuration files.

- We use ``sphinx`` as documentation generator, please follow these guidelines:

  - Add docstrings for every new module explaining the purpose and the relations to other modules (if useful).

  - Please use `reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`__ in docstrings and format them using the `Sphinx docstring format <https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html>`__.

  - Add further docstrings and comments where they are useful.
    Please avoid stating obvious things and concentrate on important / tricky code parts.

  - Extend the ``user-guide.rst`` if you add / change user-visible functionality.

  - Extend the ``developer-guide.rst`` if you add / change developer related concepts.


.. IMPORTANT::

   Many of these guidelines are checked as part of the ``audit`` make target.
   Please use it regularly before you push your changes.


Build Script and Pipeline
-------------------------

The build script automates common tasks and important checks.
In addition, the build pipeline performs those checks before new changes are introduced into the main development branch.

Build Script
^^^^^^^^^^^^

In general, you invoke the build script as follows:

``make <BUILD TARGET NAME>``


The following build targets are available:

- **clean** - Cleans up temporary created directories and files.

- **docs** - Creates the documentation.
  The result is available in ``build/html``.

- **tests** - Executes the tests.
  The results are shown on the command-line.
  In addition, the results are made available as XML report for the build pipeline (in ``build/tests.xml``).

- **formatting** - Formats the code using ``black`` and ``isort``.

- **audit** - Runs all tests, calculates the code coverage, and checks the code.
  The build target will succeed, if:

  - All tests run successfully.

  - The code coverage is greater than 80% (coverage report is in ``build/htmlcov``).

  - ``flake8``, ``black``, ``isort``, ``bandit`` issue no problems.

  - All files contain required copyright and license information (checked via ``reuse lint``).

- **package** - Creates an installable release package.
  The package is available in the ``dist`` directory.

Pipeline
^^^^^^^^

- For every new / updated feature / release branch, the build pipeline:

  - Runs the tests.

  - Performs the code quality checks (see : ``audit`` build target).
  
  - For release branches only: It creates the release package.

- For every change of the ``master`` branch, the build pipeline:

  - Creates an installable package which is marked as development version.

  - Makes the latest in-development package including its documentation available. 
